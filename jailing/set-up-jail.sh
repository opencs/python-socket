# Extract the python jail files into /srv/python3jail
tar -xf python3jail.tar.gz /srv/

# create a new read-only jail at /srv/pyjail
mkdir -p /srv/pyjail
mount --bind /srv/python3jail /srv/pyjail
mount -o remount,ro /srv/pyjail
# It needs a rw-tmpfs for shared memory between processes
mount -t tmpfs -o rw,noexec,nosuid,size=10%,mode=0777 tmpfs /srv/pyjail/run/shm

# This is the command to run the service
PYTHONHOME=/ chroot pyjail python3 /srv/server.py --port=7000 --address=0.0.0.0
