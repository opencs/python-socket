#!/usr/bin/env python3
import tornado.ioloop
import tornado.web as web
import tornado.websocket as ws
import tornado.gen as gen
import tornado.httpserver

import multiprocessing as mp
from os import setpriority
from concurrent.futures import ThreadPoolExecutor
from time import sleep

from captureio import InStream, OutStream, Capturing
from io import StringIO
import json

from queue import Queue, Empty


# Configuration
from tornado.options import define, options, parse_command_line

define("port", default=8888, help="run on the given port", type=int)
define("address", default="localhost", help="ip address to host on", type=str)

import sys
import traceback


def format_tb():
    e = sys.exc_info()
    result = []
    if e[0]:
        lines = traceback.format_exception(e[0],e[1],e[2])
        result.append(lines[0])
        # basically, just hold of printing until we get 
        # to the initial call from the user input
        # XXX this will reveal state in some cases
        print_rest = False
        for l in lines[1:]:
            if l.find('<string>')>=0: 
                print_rest = True
            if print_rest:
                result.append(l)
    return result

def print_tb():
    lines = format_tb()
    for l in lines:
        sys.stderr.write(l)


## Actual server code

class PythonWebSocket(ws.WebSocketHandler):
    def open(self):
        print("Python WebSocket opened")
        self._out = OutStream(self, {'type':'out'}, 'content')
        self._err = OutStream(self, {'type':'err'}, 'content')
        self._in = InStream(self.send_input_request)
        # XXX 4 threads should be enough to handle executing, testing, and listening for interrupts
        self.thread_pool = ThreadPoolExecutor(4)
        # Run some blank code to prep _process, and maybe more
        self.process_queue = Queue()

    def check_origin(self, origin):
        return True

    def on_message(self, message):
        data = json.loads(message)
        if data['type'] == 'exec':
            self.stream_code(data['code'])
        elif data['type'] == 'eval':
            # XXX eval not implemented
            pass
        elif data['type'] == 'input':
            self._in.add_input(data['content'])
        elif data['type'] == 'iotest':
            self.run_io_tests(data['code'], data['inputs'], data['outputs'])
        elif data['type'] == 'ftest':
            errors = self.run_f_test(data['code'], data['testCode'])
        elif data['type'] == 'interrupt':
            self.kill_process('Code Interrupt')
        else:
            self.send_error("Unsupported message type")
    
    def run_code(self, code, timeout=60):
        """ Spawn a process to run the python code block
        the timeout is how long to wait for the process to finish before terminating
        """
        p = mp.Process(target=self.code_wrapper, args=(code,))
        self.process_queue.put(p)
        p.start()
        p.join(timeout)
        if p.is_alive():
            p.terminate()
            self.send_finished(e=True, s='execution timed out')
    
    def kill_process(self, reason="", error=True, silent=False):
        """ Kill all processes in the queue
        """
        interrupted = False
        while not self.process_queue.empty():
            p = self.process_queue.get()
            if p.is_alive():
                interrupted = True
            p.terminate()
            # Block until it is properly terminated
            while p.is_alive():
                sleep(.001)
        if interrupted and not silent:
            self.send_finished(e=error, s=reason)

    def code_wrapper(self, code):
        """ wrap the executing code in the stdin/out/err capturer and tracebacks
        """
        setpriority(0,0,20)
        try:
            self._in.clear_input()
        except (Empty):
            self.send_error("error clearing the input queue")
        
        with Capturing(self._out, self._in, self._err):
            try:
                exec(code, {})
                self.send_finished()
            except (OSError):
                self.send_error('internal error')
            except (SyntaxError):
                print_tb()
                self.send_finished(True, 'There is a problem with the syntax')
            except:
                print_tb()
                self.send_finished(True, 'There was an error')

    @gen.coroutine
    def stream_code(self, code):
        """ Function to handle running code and capturing output
        Output gets captured into a stream
        """
        self.kill_process('Interrupted to run new code', silent=True)
        # Since the queue can become corrupted when execution is stopped
        self._in._queue = mp.Queue()
        yield self.thread_pool.submit(self.run_code, code, 20)
    
    @gen.coroutine
    def run_io_tests(self, code, inputs, outputs):
        """ Function to handle running stdio tests
        """
        f =  self.thread_pool.submit(self._spawn_io_tests, code, inputs, outputs)
        yield f
        results = f.result()
        msg = {
                'type': 'io-result',
                'results': results,
                }
        success = True;
        for r in results:
            success = success and r
        msg['success'] = success
        self.write_message(json.dumps(msg))
        
    def _spawn_io_tests(self, code, inputs, outputs, timeout=1):
        """ Spawn a process to run the python code block and test io
        the timeout is how long to wait for the process to finish before terminating
        """
        q = mp.Queue()
        process = mp.Process(target=self._io_tests, args=(code,inputs,outputs,q,))
        self.process_queue.put(process)
        process.start()
        process.join(timeout)
        if process.is_alive():
            process.terminate()
            self.send_error("IO tests timed out")
            return [False]
        else:
            return q.get()

    def _io_tests(self, code, inputs, outputs, q):
        results = []
        for i, o in zip(inputs, outputs):
            sin = StringIO(i)
            sout = StringIO()
            # to suppress prompts from IO tests, we override the input function
            def input_override(*args):
                return globals()['__builtins__'].input()
            with Capturing(sout, sin, StringIO()):
                try:
                    exec(code, {'input':input_override})
                except:
                    # There was an error raised in the submitted code
                    self.send_error("IO_TEST: %s" % repr(sys.exc_info()[1]))
                    results.append(False)
                    q.put(results)
                    return

                # XXX compared outputs are stripped in case the test suite doesn't include newlines
                if sout.getvalue().strip() == o.strip():
                    results.append(True)
                else:
                    results.append(False)
        q.put(results)
        return

    @gen.coroutine
    def run_f_test(self, code, asserts, timeout=1):
        """ Run the tests in a thread and wait for them to finish
        """
        f = yield self.thread_pool.submit(self._spawn_f_test, code, asserts, timeout)
        errors = f
        msg = {
                'type': 'f-result',
                }
        if errors[0] == 'traceback':
            msg['success'] = False;
            msg['traceback'] = errors[1]
        if errors[0] == 'result':
            msg['results'] = errors[1]
            if errors[1]:
                msg['success'] = False;
            else:
                msg['success'] = True;
        self.write_message(json.dumps(msg))
    
    def _spawn_f_test(self, code, asserts, timeout=1):
        """ Spawn a process to run the python code block then test it against assert statements
        the timeout is how long to wait for the process to finish before terminating
        """
        q = mp.Queue()
        process = mp.Process(target=self._f_test, args=(code,asserts,q))
        self.process_queue.put(process)
        process.start()
        process.join(timeout)
        if process.is_alive():
            process.terminate()
            self.send_error("timeout while testing the function")
            return ['Timeout during the tests']
        else:
            return q.get()

    def _f_test(self, code, test_code, q):
        g = {}
        errors = []
        ps = test_code.split("#tests", 1)
        if len(ps) == 1:
            preamble = ""
            asserts = ps[0]
        elif len(ps) == 2:
            preamble = ps[0]
            asserts = ps[1]

        with Capturing(StringIO(), StringIO(), StringIO()):
            try:
                exec(code, g)
            except:
                # There was an error raised in the submitted code
                q.put(('result', ["There was an error executing your code."]))
                return

            # Set up the testing environment
            try:
                exec(preamble, g)
            except:
                # There was an error raised in the preamble code
                q.put(('traceback', "Error while setting up tests:\n%s" % ''.join(format_tb())))
                return

            # test each assert statement
            try: 
                count = 0
                for line in asserts.splitlines():
                    count = count + 1
                    try:
                        exec(line, g)
                    except AssertionError as a:
                        errors.append(str(a))
            except:
                q.put(('traceback', 
                    "Error while running tests:\n%s\nDuring test %d:\n `%s`" % (''.join(format_tb()), 
                                                                                      count, 
                                                                                      line)))
                return
        q.put(('result', errors))
        return

    def on_close(self):
        print("Python WebSocket Closed")

    def send_error(self, s):
        msg = {
                'type': 'error',
                'msg':  s,
                }
        self.write_message(json.dumps(msg))
    
    def send_finished(self, e=False, s='No errors detected'):
        msg = {
                'type': 'stop',
                'error': e,
                'msg': s,
                }
        self.write_message(json.dumps(msg))


    def send_input_request(self):
        msg = {
                'type': 'input-req',
                }
        self.write_message(json.dumps(msg))

class PingHandler(web.RequestHandler):
    def get(self):
        self.write("pong")


def make_app():
    return web.Application([
                (r"/", PythonWebSocket),
                (r"/ping/?", PingHandler),
                ])

import logging
def main():
    parse_command_line()
    print("Opening server at http://%s:%s" % (options.address, options.port))
    # XXX we're silencing the ping request logs by changing the log level.
    # Unfortunately, the app handles all logging and not the individual handlers.
    access = logging.getLogger('tornado.access')
    access.setLevel(logging.ERROR)
    app = make_app()
    server = tornado.httpserver.HTTPServer(app)
    server.bind(options.port, address=options.address)
    server.start(0) # forks one process per cpu
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
