import sys

import multiprocessing as mp
import json

from io import TextIOBase

class InStream(TextIOBase):
    """ An implementation of a stream that sends a request for input and waits to hear back
    """
    def __init__(self, send_request):
        """ send_request is a function (provided by an outside interface) that sends out a request for input
        """
        self.send = send_request
        self._queue = mp.Queue()

    def readline(self):
        # we don't want to send a request if there's already an input available
        if self._queue.empty(): 
            self.send()
        # If the \n isn't trainling an empty string, an EOF error will be raised
        return self._queue.get(True) + '\n'

    def add_input(self, s):
        self._queue.put(s)

    def clear_input(self):
        while not self._queue.empty():
            self._queue.get_nowait()


class OutStream(TextIOBase):
    """ A wrapper class for making connections have basic StringIO Output capabilities

    - The messages are wrapped in json so that they can be used directly in the browser
    """

    def __init__(self, conn, template, output_key='content'):
        self._conn=conn
        self._key = output_key
        self._template = template

    def write(self, s):
        result = self._template
        result.update({
                'content': s,
                })
        self._conn.write_message(json.dumps(result))

    def flush(self):
        """ Flush may be inportant later """
        pass

class Capturing:
    """ Context class to capture stdout and stderr into streams
    """

    def __init__(self, outStream, inStream, errStream):
        self._out = outStream
        self._in = inStream
        self._err = errStream

    def __enter__(self):
        self._stdout = sys.stdout
        self._stdin = sys.stdin
        self._stderr = sys.stderr
        sys.stdout = self._out
        sys.stdin = self._in
        sys.stderr = self._err
        return self

    def __exit__(self, *args):
        sys.stderr = self._stderr
        sys.stdin = self._stdin
        sys.stdout = self._stdout
