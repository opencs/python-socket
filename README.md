python-server
=============

A websocket service which receives Python code, and sends back the results of execution (in stdin and stdout).It is designed to run inside of a chroot jail.

## Setup/config

Example configuration files are located in the `config/` directory. The contents of `nginx.conf` should be added to the proxy server configuration. The `pysocket*` config files are upstart service definitions which launch the pysocket services and monitor them.

## Use

All communication with the server is performed with message passing through the websocket. The message spec is in `message-spec.md` (parts may not be fully implemented).

